---
name: Live Streaming
---
Live Streaming
==============

We recorded and live streamed video from 3 venues during DebConf17.

The recordings from these events are available under a [free license][video-license]
the in the [Debian meetings archive website][meetings-archive-dc17].

There are links to these recordings (and a web video player) on the
talk pages for these events.

[video-license]: http://meetings-archive.debian.net/pub/debian-meetings/LICENSE
[meetings-archive-dc17]: http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/
