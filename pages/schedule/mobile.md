---
name: Mobile-friendly Schedule
---
Mobile-friendly Schedule
========================

The Schedule is available through an XML feed. You can use _ConfClerk_ in Debian to consume this, or _Giggity_ on Android.

![Schedule QR Code](/static/img/dc17-schedule-qr-code.png "DebConf17 Schedule")

XML Feed: [https://debconf17.debconf.org/schedule/pentabarf.xml](https://debconf17.debconf.org/schedule/pentabarf.xml "https://debconf17.debconf.org/schedule/pentabarf.xml")

Download Giggity
----------------

- Home page: [https://gaa.st/giggity](https://gaa.st/giggity "https://gaa.st/giggity")
- F-Droid: [https://f-droid.org/repository/browse/?fdid=net.gaast.giggity](https://f-droid.org/repository/browse/?fdid=net.gaast.giggity "https://f-droid.org/repository/browse/?fdid=net.gaast.giggity")
- Google Play Store: [https://play.google.com/store/apps/details?id=net.gaast.giggity
](https://play.google.com/store/apps/details?id=net.gaast.giggity "https://play.google.com/store/apps/details?id=net.gaast.giggity")

Download ConfClerk
------------------

	# apt install confclerk
