---
name: Registration Information
---

Registration Information
========================

Online registration for DebConf17 is now closed.
If you are not registered but would still like to attend, fear not!
We will accept drop-in registrations at the Front Desk beginning
Tuesday, August 1st.
There, you'll be able to get a badge, pay any applicable fees (if you're
registering as a professional or corporate attendee) and purchase a meal
card as long as these are still available (we have about 30 available
for on-sites ale).
We will, however, not be accepting any new sign-ups for
conference-provided accommodation.

Registration Fees
-----------------

As always, basic registration for DebConf is free of charge for attendees. If you are attending the conference in a professional capacity, or as a representative of your company, we ask that you consider registering in one of our paid categories, to help cover the costs of putting on the conference:

**Professional registration**: with a rate of 200 USD for the week, this covers the costs associated with your individual attendance at the conference.

**Corporate registration**: with a registration fee of 500 USD, this category is intended for those participating in DebConf as representatives of companies and helps to subsidise the cost of the conference for others.

As in previous three DebConfs, these registration fees do not include food or accommodation.

We encourage all attendees to help make DebConf a success by selecting the registration category appropriate for their situation.

Sponsored attendance
--------------------

Any member of the Debian community is welcome to request sponsorship to attend DebConf. As in previous years, we will do our best to offer sponsored food and accommodation to Debian contributors, but sponsorship is subject to demand and available funding. Decisions regarding sponsorship requests are expected by the end of May, 2017.
