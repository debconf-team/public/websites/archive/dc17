from django.conf.urls import url

from wafer.schedule.views import ScheduleView
from dc17.views import (
    RegistrationWizard, UnregisterView, AttendeeAccommExport,
    AttendeeBadgeExport, BursaryRequestExport, BursaryRefereeExport,
    BursaryRefereeAdd, BursaryMassUpdate, BursaryUpdate,
)


class OpenDayView(ScheduleView):
    def get_context_data(self, **kwargs):
        self.request.GET = self.request.GET.copy()
        self.request.GET['day'] = '2017-08-05'
        return super().get_context_data(**kwargs)


urlpatterns = [
    url(r'^register/$', RegistrationWizard.as_view(), name='register'),
    url(r'^unregister/$', UnregisterView.as_view(), name='unregister'),
    url(r'^attendees/admin/export/accomm/$', AttendeeAccommExport.as_view(),
        name='attendee_admin_export_accomm'),
    url(r'^attendees/admin/export/badges/$', AttendeeBadgeExport.as_view(),
        name='attendee_admin_export_badges'),
    url(r'^bursaries/admin/export/requests/$', BursaryRequestExport.as_view(),
        name='bursaries_admin_export_requests'),
    url(r'^bursaries/admin/export/referees/$', BursaryRefereeExport.as_view(),
        name='bursaries_admin_export_referees'),
    url(r'^bursaries/admin/add_referees/$', BursaryRefereeAdd.as_view(),
        name='bursaries_admin_add_referees'),
    url(r'^bursaries/admin/update_requests/$', BursaryMassUpdate.as_view(),
        name='bursaries_admin_update_requests'),
    url(r'^bursary/$', BursaryUpdate.as_view(), name='bursary_update'),
    url(r'^schedule/open-day/$', OpenDayView.as_view()),
]
