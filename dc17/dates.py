import datetime


def meals(orga=False):
    day = datetime.date(2017, 7, 31)
    if orga:
        day = datetime.date(2017, 7, 28)
    while day <= datetime.date(2017, 8, 13):
        yield ('breakfast', day)
        if day < datetime.date(2017, 8, 13):
            yield ('lunch', day)
            yield ('dinner', day)
        day += datetime.timedelta(days=1)


def meal_choices(orga=False):
    for meal, date in meals(orga=orga):
        date = date.isoformat()
        yield ('{}_{}'.format(meal, date),
               '{} {}'.format(meal.title(), date))


def nights(orga=False):
    day = datetime.date(2017, 7, 31)
    if orga:
        day = datetime.date(2017, 7, 28)
    while day <= datetime.date(2017, 8, 12):
        yield day
        day += datetime.timedelta(days=1)


def night_choices(orga=False):
    for date in nights(orga=orga):
        date = date.isoformat()
        yield ('night_{}'.format(date), 'Night of {}'.format(date))


def get_ranges_for_dates(dates):
    """Get ranges of consecutive dates for the given set of dates"""
    one = datetime.timedelta(days=1)

    ranges = []

    range_start = None
    range_end = None
    for date in sorted(dates):
        if range_start is None:
            range_start = date
        else:
            if date != range_end + one:
                # dates not consecutive, save old range and start new one
                ranges.append([range_start, range_end])
                range_start = date

        range_end = date

    ranges.append([range_start, range_end])
    return ranges
