# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dc17', '0008_bursary_referee_unicity'),
    ]

    operations = [
        migrations.AddField(
            model_name='bursaryreferee',
            name='final',
            field=models.BooleanField(verbose_name='Final assessment', default=False),
        ),
    ]
