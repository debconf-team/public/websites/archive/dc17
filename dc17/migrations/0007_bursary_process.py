# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings
import dc17.models.bursary


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dc17', '0006_remove_departure_night'),
    ]

    operations = [
        migrations.CreateModel(
            name='BursaryReferee',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('contrib_score', models.DecimalField(null=True, max_digits=3, decimal_places=2, validators=[dc17.models.bursary.validate_score], help_text='Blahblahblah', verbose_name='Contribution score')),
                ('outreach_score', models.DecimalField(null=True, max_digits=3, decimal_places=2, validators=[dc17.models.bursary.validate_score], help_text='Blahblahblah', verbose_name='Diversity and Inclusion score')),
                ('notes', models.TextField(help_text='Let us know how you came to your decision', verbose_name='Notes for evaluation', default='')),
            ],
        ),
        migrations.AlterModelOptions(
            name='bursary',
            options={'verbose_name': 'bursary request'},
        ),
        migrations.RemoveField(
            model_name='bursary',
            name='rank',
        ),
        migrations.RemoveField(
            model_name='bursary',
            name='team_notes',
        ),
        migrations.AddField(
            model_name='bursary',
            name='accept_before',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='bursary',
            name='status',
            field=models.TextField(choices=[('canceled', 'The bursary request has been canceled'), ('expired', 'The bursary request has expired'), ('submitted', 'The bursary request has been submitted'), ('accepted', 'The bursary request has been accepted'), ('pending', 'The bursary request has been granted and is pending your acceptance'), ('ranked', 'The bursary request has been ranked'), ('denied', 'The bursary request has been denied')], default='submitted'),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='need',
            field=models.CharField(choices=[('sacrifice', 'Without the requested funding, I will have to make financial sacrifices to attend'), ('non-financial', 'I am not applying based on financial need'), ('inconvenient', 'Without the requested funding, attending will be inconvenient for me'), ('unable', 'Without this funding, I will be absolutely unable to attend')], max_length=16),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='request',
            field=models.CharField(null=True, choices=[('food+accomm', 'Food and accommodation only'), ('travel+food+accomm', 'Travel, food and accommodation')], max_length=32),
        ),
        migrations.AddField(
            model_name='bursaryreferee',
            name='bursary',
            field=models.ForeignKey(related_name='referees', to='dc17.Bursary'),
        ),
        migrations.AddField(
            model_name='bursaryreferee',
            name='referee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
