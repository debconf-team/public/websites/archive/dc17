# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dc17', '0011_bursary_status_as_charfield'),
    ]

    operations = [
        migrations.AddField(
            model_name='attendee',
            name='invoice_generated',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='food_status',
            field=models.CharField(max_length=32, choices=[('submitted', 'The bursary request has been submitted'), ('ranked', 'The bursary request has been ranked'), ('pending', 'The bursary request has been granted and is pending your acceptance'), ('accepted', 'The bursary has been accepted'), ('denied', 'The bursary has been denied'), ('expired', 'The bursary grant has expired'), ('canceled', 'The bursary request has been canceled')], default='submitted'),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='travel_status',
            field=models.CharField(max_length=32, choices=[('submitted', 'The bursary request has been submitted'), ('ranked', 'The bursary request has been ranked'), ('pending', 'The bursary request has been granted and is pending your acceptance'), ('accepted', 'The bursary has been accepted'), ('denied', 'The bursary has been denied'), ('expired', 'The bursary grant has expired'), ('canceled', 'The bursary request has been canceled')], default='submitted'),
        ),
    ]
