# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dc17', '0010_bursary_acceptance'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bursary',
            name='food_status',
            field=models.CharField(max_length=32, default='submitted', choices=[('submitted', 'The bursary request has been submitted'), ('ranked', 'The bursary request has been ranked'), ('pending', 'The bursary request has been granted and is pending your acceptance'), ('accepted', 'The bursary request has been accepted'), ('denied', 'The bursary request has been denied'), ('expired', 'The bursary request has expired'), ('canceled', 'The bursary request has been canceled')]),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='travel_status',
            field=models.CharField(max_length=32, default='submitted', choices=[('submitted', 'The bursary request has been submitted'), ('ranked', 'The bursary request has been ranked'), ('pending', 'The bursary request has been granted and is pending your acceptance'), ('accepted', 'The bursary request has been accepted'), ('denied', 'The bursary request has been denied'), ('expired', 'The bursary request has expired'), ('canceled', 'The bursary request has been canceled')]),
        ),
    ]
