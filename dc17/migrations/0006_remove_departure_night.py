# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date

from django.core.mail import EmailMultiAlternatives
from django.db import migrations


def email(user):
    body = '''Hi,

Whoops, we made a mistake with the DebConf registration form, and let you
register for accommodation on the night of August 13th.

That's not actually going to be available. DebConf runs until the 12th, so the
latest departure from the provided accommodation will be on the morning of the
13th, after breakfast.

We have updated your registration, accordingly.

Sorry about that,

The DebConf 17 registration team.
'''
    to = user.email
    subject = '[DebConf 17] Registration: Night of August 13th'
    email_message = EmailMultiAlternatives(subject, body, to=[to])
    email_message.send()
    print("Emailed %s about accomm on Aug 13th" % user.username)


def remove_night(apps, schema_editor):
    AccommNight = apps.get_model('dc17', 'AccommNight')

    try:
        night = AccommNight.objects.get(date=date(2017, 8, 13))
    except AccommNight.DoesNotExist:
        night = None

    if night:
        for accomm in night.accomm_set.all():
            email(accomm.attendee.user)
        night.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('dc17', '0005_move_nights_to_2017'),
    ]
    operations = [
        migrations.RunPython(remove_night),
    ]
