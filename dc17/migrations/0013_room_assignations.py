# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dc17', '0012_auto_20170705_0448'),
    ]

    operations = [
        migrations.AddField(
            model_name='accomm',
            name='onsite_room',
            field=models.CharField(max_length=128, blank=True, default=''),
        ),
        migrations.AddField(
            model_name='accomm',
            name='rvc_room',
            field=models.CharField(max_length=128, blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='accomm',
            name='alt_accomm_choice',
            field=models.CharField(null=True, blank=True, max_length=16),
        ),
        migrations.AlterField(
            model_name='accomm',
            name='childcare_details',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='accomm',
            name='childcare_needs',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='accomm',
            name='family_usernames',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='accomm',
            name='requirements',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='accomm',
            name='special_needs',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='arrival',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='billing_address',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='country',
            field=models.CharField(max_length=2, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='departure',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='emergency_contact',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='fee',
            field=models.CharField(max_length=5, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='gender',
            field=models.CharField(max_length=1, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='languages',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='nametag_2',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='nametag_3',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='notes',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='t_shirt_cut',
            field=models.CharField(max_length=1, blank=True),
        ),
        migrations.AlterField(
            model_name='attendee',
            name='t_shirt_size',
            field=models.CharField(max_length=8, blank=True),
        ),
        migrations.AlterField(
            model_name='food',
            name='diet',
            field=models.CharField(max_length=16, blank=True),
        ),
        migrations.AlterField(
            model_name='food',
            name='special_diet',
            field=models.TextField(blank=True),
        ),
    ]
