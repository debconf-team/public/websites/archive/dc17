# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def renumber_nights(apps, schema_editor):
    AccommNight = apps.get_model('dc17', 'AccommNight')
    for night in AccommNight.objects.all():
        night.date = night.date.replace(year=2017)
        night.save()


class Migration(migrations.Migration):
    dependencies = [
        ('dc17', '0004_travel_bursary_null'),
    ]
    operations = [
        migrations.RunPython(renumber_nights),
    ]
