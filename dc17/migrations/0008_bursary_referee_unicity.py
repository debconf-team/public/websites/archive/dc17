# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import dc17.models.bursary


class Migration(migrations.Migration):

    dependencies = [
        ('dc17', '0007_bursary_process'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bursary',
            name='accept_before',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='approved_travel',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='need',
            field=models.CharField(choices=[('unable', 'Without this funding, I will be absolutely unable to attend'), ('sacrifice', 'Without the requested funding, I will have to make financial sacrifices to attend'), ('inconvenient', 'Without the requested funding, attending will be inconvenient for me'), ('non-financial', 'I am not applying based on financial need')], max_length=16),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='reimbursed_amount',
            field=models.IntegerField(default=0, blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='status',
            field=models.TextField(default='submitted', choices=[('submitted', 'The bursary request has been submitted'), ('ranked', 'The bursary request has been ranked'), ('pending', 'The bursary request has been granted and is pending your acceptance'), ('accepted', 'The bursary request has been accepted'), ('denied', 'The bursary request has been denied'), ('expired', 'The bursary request has expired'), ('canceled', 'The bursary request has been canceled')]),
        ),
        migrations.AlterField(
            model_name='bursaryreferee',
            name='contrib_score',
            field=models.DecimalField(validators=[dc17.models.bursary.validate_score], help_text='\n\nDecimal value from 0 to 5; Please try to conform to the following scale for\nyour assessment.\n\n<dl>\n<dt>5 "Must fund"</dt>\n<dd>\nIf this person does not attend DebConf, there will be significant\nnegative impact for DebConf or Debian more generally.\n</dd>\n<dt>4 "Priority funding"</dt>\n<dd>\nThere are clear benefits to Debian or to DebConf of this person attending\ndebconf. This might be an accepted talk that seems particularly important.\n</dd>\n<dt>3 "Good initiative"</dt>\n<dd>\nWe should fund this person because they propose something interesting.\n</dd>\n<dt>2 "Good record"</dt>\n<dd>\nThis person has a record of substantial contribution to Debian.this should\ngenerally be recent contribution, i.e. within the last two years.\n</dd>\n<dt>1 "OK"</dt>\n<dd>\nIf we have budget, I don\'t object to funding this request.\n</dd>\n<dt>0 "Deny"</dt>\n<dd>\nEven if we have budget, I think we should not fund this request.\n</dd>\n</dl>\n', null=True, blank=True, verbose_name='Contribution score', decimal_places=2, max_digits=3),
        ),
        migrations.AlterField(
            model_name='bursaryreferee',
            name='notes',
            field=models.TextField(default='', help_text='Let us know how you came to your decision', blank=True, verbose_name='Notes for evaluation'),
        ),
        migrations.AlterField(
            model_name='bursaryreferee',
            name='outreach_score',
            field=models.DecimalField(validators=[dc17.models.bursary.validate_score], help_text='\n\nDecimal value from 0 to 5; Please try to assess applicants according to the\nfollowing criteria, and to use the full scale across your (non-null) ratings.\n\n<ul>\n<li>Gender (favoring women (cis, trans, queer), non-binary and genderqueer\nindividuals, as well as trans men)</li>\n<li>Age (favoring individuals over 35 years old)</li>\n<li>Country of origin / ethnic diversity</li>\n<li>Whether the applicant is a newcomer to Debian (yes should rank higher)</li>\n</ul>\n\n', null=True, blank=True, verbose_name='Diversity and Inclusion score', decimal_places=2, max_digits=3),
        ),
        migrations.AlterUniqueTogether(
            name='bursaryreferee',
            unique_together=set([('bursary', 'referee')]),
        ),
    ]
