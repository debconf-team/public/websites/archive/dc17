# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dc17', '0009_bursary_evaluation_add_final'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bursary',
            old_name='accept_before',
            new_name='food_accept_before',
        ),
        migrations.RenameField(
            model_name='bursary',
            old_name='status',
            new_name='food_status',
        ),
        migrations.RemoveField(
            model_name='bursary',
            name='approved_accomm',
        ),
        migrations.RemoveField(
            model_name='bursary',
            name='approved_travel',
        ),
        migrations.AddField(
            model_name='bursary',
            name='accommodation_needed',
            field=models.NullBooleanField(),
        ),
        migrations.AddField(
            model_name='bursary',
            name='food_needed',
            field=models.NullBooleanField(),
        ),
        migrations.AddField(
            model_name='bursary',
            name='travel_accept_before',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bursary',
            name='travel_status',
            field=models.TextField(choices=[('submitted', 'The bursary request has been submitted'), ('ranked', 'The bursary request has been ranked'), ('pending', 'The bursary request has been granted and is pending your acceptance'), ('accepted', 'The bursary request has been accepted'), ('denied', 'The bursary request has been denied'), ('expired', 'The bursary request has expired'), ('canceled', 'The bursary request has been canceled')], default='submitted'),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='need',
            field=models.CharField(choices=[('unable', 'Without this funding, I will be absolutely unable to attend'), ('sacrifice', 'Without the requested funding, I will have to make financial sacrifices to attend'), ('inconvenient', 'Without the requested funding, attending will be inconvenient for me'), ('non-financial', 'I am not applying based on financial need')], max_length=16, blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='reason_contribution',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='reason_diversity',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='reason_plans',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='request',
            field=models.CharField(choices=[('food+accomm', 'Food and accommodation only'), ('travel+food+accomm', 'Travel, food and accommodation')], null=True, max_length=32, blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='travel_bursary',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bursary',
            name='travel_from',
            field=models.TextField(blank=True),
        ),
    ]
