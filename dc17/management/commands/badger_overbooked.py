# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.db.models import Q

from dc17.models.accommodation import Accomm
from dc17.models.attendee import Attendee
from dc17.models.bursary import Bursary

SUBJECT = 'DebConf17: Accommodation Overbooked - RVC rooms available'

TXT = """\
Dear %(name)s,

DebConf draws ever closer.  More importantly, it is proving more popular than
expected, to the point that the on-site accommodation is somewhat overbooked.
Therefore we are inviting you to stay at the McGill Royal Victoria College
dormitory instead of in the on-site accommodation, between August 5 and 12, at
no extra cost to yourself.

If you accept, you will get a room of your own, with a bed, shared bathroom and
kitchenette facilities.  The trade-off is that it's a bit further from the
venue, but we're able to reimburse the cost of a 7 day public transit pass to
those staying at RVC who request it.  You'll find details about the
accommodation at RVC on the wiki:
https://wiki.debconf.org/wiki/DebConf17/Accommodation#McGill_Royal_Victoria_College_Residence

To sign up, simply reply to this email before July 16.

See you in Montréal,
The DebConf Registration team
"""


class Command(BaseCommand):
    help = 'Offer alternative accomm to self-payers'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, attendee, dry_run):
        try:
            if attendee.user.bursary.request:
                return
        except Bursary.DoesNotExist:
            pass
        try:
            if attendee.accomm.alt_accomm_choice is not None:
                return
        except Accomm.DoesNotExist:
            return

        to = attendee.user.email
        if dry_run:
            print('I would badger: %s <%s>' % (
                attendee.user.userprofile.display_name(), to))
            return

        body = TXT % {
            'name': attendee.user.userprofile.display_name(),
        }
        email_message = EmailMultiAlternatives(SUBJECT, body, to=[to])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        for attendee in Attendee.objects.filter(
            Q(debcamp=True) | Q(open_day=True) | Q(debconf=True),
        ):
            self.badger(attendee, dry_run)
