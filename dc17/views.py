import collections
import csv
import json
import logging

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic import FormView, ListView, TemplateView, UpdateView

from django.contrib.auth.decorators import permission_required
from django.contrib.messages.views import SuccessMessageMixin

from django_countries import countries

from formtools.wizard.views import SessionWizardView
from wafer.utils import LoginRequiredMixin

from dc17.forms import (REGISTRATION_FORMS, BursaryRefereeAddForm,
                        BursaryUpdateForm, BursaryMassUpdateForm)
from dc17.models import Accomm, Attendee, Bursary, BursaryReferee

FEES = {
    '': 0,
    'pro':  200,
    'corp': 500
}


class RegistrationWizard(LoginRequiredMixin, SessionWizardView):
    form_list = REGISTRATION_FORMS

    def get_template_names(self):
        if self.steps.step1 == self.steps.count:
            return 'dc17/confirmation_form.html'
        else:
            return 'dc17/registration_form.html'

    def get_context_data(self, form, **kwargs):
        context = super(RegistrationWizard, self).get_context_data(
            form=form,
            **kwargs)
        if self.steps.step1 == self.steps.count:
            context_update = {}
            for form, step in self.form_list.items():
                cleaned_data = self.get_cleaned_data_for_step(form)

                if cleaned_data is None:
                    continue

                for key, value in cleaned_data.items():
                    context_update[key] = value

            context.update(context_update)

            meals_by_day = {}
            meals_by_type = {
                'breakfast': 0,
                'dinner': 0,
                'lunch': 0,
            }

            for selection in context_update.get('meals'):
                (meal, day) = tuple(selection.split('_', 2))

                if meals_by_day.get(day, None) is None:
                    meals_by_day[day] = []
                meals_by_day[day] += [meal]
                meals_by_type[meal] += 1

            meals_summary = sorted([
                day + ': ' + (', '.join(meals))
                for day, meals in meals_by_day.items()
            ])

            context.update({
                'nametag': '{}\n{}\n{}'.format(
                    context_update.get('name'),
                    context_update.get('nametag_2'),
                    context_update.get('nametag_3')
                ),

                'genders': {
                    'm': 'Male',
                    'f': 'Female',
                },
                'country_name': dict(countries).get(
                    context_update.get('country'), None
                ),

                'meals_summary': meals_summary,
                'meals_by_type': meals_by_type,
                'food_price_by_type': {
                    'breakfast': '{} breakfast(s) * 3 CAD$ = {:.2f}'
                    ' CAD$'.format(
                        meals_by_type.get('breakfast'),
                        meals_by_type.get('breakfast') * 3
                    ),
                    'lunch': '{} lunch(es) * 7.50 CAD$ = {:.2f} CAD$'.format(
                        meals_by_type.get('lunch'),
                        meals_by_type.get('lunch') * 7.5
                    ),
                    'dinner': '{} dinner(s) * 7.50 CAD$ = {:.2f} CAD$'.format(
                        meals_by_type.get('dinner'),
                        meals_by_type.get('dinner') * 7.5
                    )
                },

                'accomm_nights_summary': ', '.join(
                    [n[6:] for n in context_update.get('nights')]
                ),
                'accomm_total': "{} night(s) * 30 CAD$ = {:.2f} CAD$".format(
                    len(context_update.get('nights')),
                    len(context_update.get('nights')) * 30
                ),

                'fee_value': '{:.2f}'.format(FEES[context_update.get('fee')]),

                'total_due': len(context_update.get('nights')) * 30 +
                meals_by_type.get('breakfast') * 3 +
                meals_by_type.get('lunch') * 7.5 +
                meals_by_type.get('dinner') * 7.5 +
                FEES[context_update.get('fee')]

            })

        return context

    def get_form_initial(self, step):
        initial = super().get_form_initial(step)
        user = self.request.user
        form = self.form_list[step]
        initial.update(form.get_initial(user))
        return initial

    def get_form_kwargs(self, step):
        kwargs = super().get_form_kwargs(step)
        kwargs['wizard'] = self
        return kwargs

    def done(self, form_list, **kwargs):
        user = self.request.user
        attendee_data = {}
        for form in form_list:
            attendee_data.update(form.get_attendee_data())

        if not settings.WAFER_REGISTRATION_OPEN:
            return render(self.request, 'dc17/registration_closed.html')

        attendee, fresh_registration = Attendee.objects.update_or_create(
            user=user, defaults=attendee_data)

        for form in form_list:
            form.save(user, attendee)

        if attendee_data.get('reconfirm') and not attendee.invoice_generated:
            try:
                attendee.generate_invoice(save=True)
            except ValueError:
                # exceptional cases (>_> tumbleweed) to be handled by hand
                pass

        self.log_registration(user, fresh_registration)

        context = {
            'fresh_registration': fresh_registration,
        }
        self.send_registered_email(user, context)
        return render(self.request, 'dc17/registration_received.html',
                      context)

    def send_registered_email(self, user, context):
        txt = render_to_string('dc17/registration_received_email.txt',
                               context)
        to = user.email
        if context['fresh_registration']:
            subject = '[DebConf 17] Registration received'
        else:
            subject = '[DebConf 17] Registration updated'
        email_message = EmailMultiAlternatives(subject, txt, to=[to])
        email_message.send()

    def log_registration(self, user, fresh_registration):
        log = logging.getLogger('dc17.registration')
        form_data = json.dumps(self.get_all_cleaned_data(),
                               cls=DjangoJSONEncoder)
        log.info('User registered: user=%s updated=%s data=%s',
                 user.username, not fresh_registration, form_data)


class UnregisterView(LoginRequiredMixin, TemplateView):
    template_name = 'dc17/unregister.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'registered': Attendee.objects.filter(user=self.request.user)
                                  .exists(),
            'bursary': Bursary.objects.filter(user=self.request.user).exists(),
        })
        return context

    def post(self, request, *args, **kwargs):
        user = self.request.user
        if user.attendee:
            user.attendee.delete()
        bursary = Bursary.objects.filter(user=self.request.user)
        if bursary:
            bursary.update(request=None)

        context = self.get_context_data()
        return self.render_to_response(context)


class BursaryAdminMixin(LoginRequiredMixin):
    @method_decorator(permission_required('dc17.add_bursaryreferee'))
    def dispatch(self, *args, **kwargs):
        return super(BursaryAdminMixin, self).dispatch(*args, **kwargs)


class CSVExportView(ListView):
    """Export the given columns for the model as CSV."""
    columns = None
    filename = None

    def get_data_line(self, instance):
        ret = []
        for column in self.columns:
            obj = instance
            for component in column.split('.'):
                try:
                    obj = getattr(obj, component)
                except ObjectDoesNotExist:
                    obj = '%s missing!' % component
                    break
                if not obj:
                    break
                if callable(obj):
                    obj = obj()
            if (not isinstance(obj, (str, bytes))
                    and isinstance(obj, collections.Iterable)):
                ret.extend(str(i) for i in obj)
            else:
                ret.append(str(obj))

        return ret

    def render_to_response(self, context, **response_kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = (
            'attachment; filename="%s"' % self.filename
        )

        writer = csv.writer(response)
        writer.writerow(self.columns)
        for instance in context['object_list']:
            writer.writerow(self.get_data_line(instance))

        return response


class BursaryRequestExport(BursaryAdminMixin, CSVExportView):
    model = Bursary
    filename = "bursaries.csv"
    ordering = ('user__username',)
    columns = [
        'user.username', 'user.email', 'user.first_name', 'user.last_name',
        'user.attendee.debcamp', 'user.attendee.open_day',
        'user.attendee.debconf', 'user.attendee.arrival',
        'user.attendee.departure', 'user.attendee.final_dates',
        'user.attendee.reconfirm', 'request', 'reason_contribution',
        'reason_plans', 'reason_diversity', 'need', 'travel_bursary',
        'travel_from', 'food_status', 'food_accept_before', 'travel_status',
        'travel_accept_before', 'food_needed', 'accommodation_needed',
        'user.attendee.accomm.alt_accomm_choice', 'reimbursed_amount',
    ]


class BursaryRefereeExport(BursaryAdminMixin, CSVExportView):
    model = BursaryReferee
    filename = "bursary_referees.csv"
    ordering = ('bursary__user__username', 'referee__username')
    columns = [
        'bursary.user.username', 'referee.username', 'final', 'contrib_score',
        'outreach_score', 'notes',
    ]


class BursaryRefereeAdd(BursaryAdminMixin, FormView):
    form_class = BursaryRefereeAddForm
    template_name = 'dc17/bursaries/add_referees.html'
    success_url = reverse_lazy('admin:dc17_bursaryreferee_changelist')

    def form_valid(self, form):
        new_referees = form.cleaned_data['to_add']
        referee_users = form.cleaned_data['referees']
        bursary_requests = form.cleaned_data['bursaries']
        for requestor, referees in new_referees.items():
            for referee in referees:
                BursaryReferee.objects.get_or_create(
                    bursary=bursary_requests[requestor],
                    referee=referee_users[referee],
                )
        return super(BursaryRefereeAdd, self).form_valid(form)


class BursaryMassUpdate(BursaryAdminMixin, FormView):
    form_class = BursaryMassUpdateForm
    template_name = 'dc17/bursaries/add_referees.html'
    success_url = reverse_lazy('admin:dc17_bursary_changelist')

    def form_valid(self, form):
        bursaries = form.cleaned_data['bursaries']
        notifies = form.cleaned_data['notifies']

        for bursary in bursaries:
            bursary.save()
            if bursary in notifies:
                bursary.notify_status(self.request)

        return super(BursaryMassUpdate, self).form_valid(form)


class BursaryUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Bursary
    form_class = BursaryUpdateForm
    template_name = 'dc17/bursaries/update.html'
    success_url = reverse_lazy('bursary_update')
    success_message = 'Bursary request updated successfully'

    def get_object(self):
        try:
            bursary = Bursary.objects.get(user=self.request.user,
                                          request__isnull=False)
        except Bursary.DoesNotExist:
            raise Http404('No bursary request submitted')
        else:
            if not bursary.can_update():
                raise Http404('Bursary cannot be updated')
            return bursary


class AttendeeAdminMixin(LoginRequiredMixin):
    @method_decorator(permission_required('dc17.change_attendee'))
    def dispatch(self, *args, **kwargs):
        return super(AttendeeAdminMixin, self).dispatch(*args, **kwargs)


class AttendeeBadgeExport(AttendeeAdminMixin, CSVExportView):
    model = Attendee
    filename = "attendee_badges.csv"
    ordering = ('user__username',)
    columns = [
        'user.username', 'reconfirm', 'user.email', 'user.get_full_name',
        'nametag_2', 'nametag_3', 'languages', 'food.diet',
    ]


class AttendeeAccommExport(AttendeeAdminMixin, CSVExportView):
    model = Accomm
    filename = "attendee_accommodation.csv"
    ordering = ('attendee__user__username',)
    columns = [
        'attendee.user.username', 'attendee.user.get_full_name',
        'attendee.user.email', 'attendee.reconfirm',
        'attendee.user.bursary.food_status', 'alt_accomm_choice',
        'requirements', 'special_needs', 'family_usernames',
        'get_checkin_checkouts', 'onsite_room', 'rvc_room',
    ]
