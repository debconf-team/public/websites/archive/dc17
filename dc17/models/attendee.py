from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone

from invoices.models import Invoice, InvoiceLine


class Attendee(models.Model):
    FEE_CHOICES = (
        ('regular', 'Regular - Free'),
        ('pro', 'Professional - 200 CAD'),
        ('corp', 'Corporate - 500 CAD'),
    )

    FEE_INVOICE_INFO = {
        'pro': {
            'reference': 'DC17-REG-PRO',
            'description': 'DebConf 17 Professional registration fee',
            'unit_price': 200,
        },
        'corp': {
            'reference': 'DC17-REG-CORP',
            'description': 'DebConf 17 Corporate registration fee',
            'unit_price': 500,
        },
    }

    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='attendee')

    # Contact information
    nametag_2 = models.CharField(max_length=50, blank=True)
    nametag_3 = models.CharField(max_length=50, blank=True)
    emergency_contact = models.TextField(blank=True)
    announce_me = models.BooleanField()
    register_announce = models.BooleanField()
    register_discuss = models.BooleanField()

    # Conference details
    debcamp = models.BooleanField()
    open_day = models.BooleanField()
    debconf = models.BooleanField()
    fee = models.CharField(max_length=5, blank=True)
    arrival = models.DateTimeField(null=True, blank=True)
    departure = models.DateTimeField(null=True, blank=True)
    final_dates = models.BooleanField()
    reconfirm = models.BooleanField()

    # Personal information
    t_shirt_cut = models.CharField(max_length=1, blank=True)
    t_shirt_size = models.CharField(max_length=8, blank=True)
    gender = models.CharField(max_length=1, blank=True)
    country = models.CharField(max_length=2, blank=True)
    languages = models.CharField(max_length=50, blank=True)

    # Misc
    billing_address = models.TextField(blank=True)
    invoice_generated = models.BooleanField(default=False)
    notes = models.TextField(blank=True)

    def __str__(self):
        return 'Attendee <{}>'.format(self.user.username)

    def generate_invoice(self, force=False, save=False):
        if not force and not (self.debcamp or self.debconf or self.open_day):
            raise ValueError('Attendee not registered: %s' % self.user)

        try:
            bursary = self.user.bursary
        except ObjectDoesNotExist:
            pass
        else:
            if not force and bursary.request:
                raise ValueError('Attendee has made a bursary request: %s'
                                 % self.user)

        lines = []
        fee_info = self.FEE_INVOICE_INFO.get(self.fee)
        if fee_info:
            fee_info['quantity'] = 1
            lines.append(InvoiceLine(**fee_info))

        try:
            accomm = self.accomm
        except ObjectDoesNotExist:
            pass
        else:
            for line in accomm.invoice_data():
                lines.append(InvoiceLine(**line))

        try:
            food = self.food
        except ObjectDoesNotExist:
            pass
        else:
            for line in food.invoice_data():
                lines.append(InvoiceLine(**line))

        invoice = Invoice(
            recipient=self.user,
            status='draft',
            date=timezone.now(),
            billing_address=self.billing_address
        )

        # Only save invoices if non empty
        if save and lines:
            invoice.save()
            self.invoice_generated = True
            self.save()

        for i, line in enumerate(lines):
            line.line_order = i
            if save:
                invoice.lines.add(line)

        return invoice, lines

    def paid(self):
        invoices = self.user.invoices
        if invoices.filter(status='draft').exists():
            return False
        if invoices.exists():
            return True
        return None

    paid.boolean = True

    def save(self, *args, **kwargs):
        if self.arrival == '':
            self.arrival = None
        if self.departure == '':
            self.departure = None
        return super().save(*args, **kwargs)
