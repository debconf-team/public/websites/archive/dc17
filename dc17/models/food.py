from decimal import Decimal

from django.db import models

from dc17.dates import get_ranges_for_dates
from dc17.models.attendee import Attendee


class Meal(models.Model):
    MEAL_PRICES = {
        'breakfast': Decimal(3),
        'lunch': Decimal('7.50'),
        'dinner': Decimal('7.50'),
    }

    MEAL_CHOICES = (
        ('breakfast', 'Breakfast'),
        ('lunch', 'Lunch'),
        ('dinner', 'Dinner'),
    )

    date = models.DateField(db_index=True)
    meal = models.CharField(max_length=16)

    @property
    def form_name(self):
        return '{}_{}'.format(self.meal, self.date.isoformat())

    def __str__(self):
        return '{}: {}'.format(self.date.isoformat(), self.meal)

    class Meta:
        unique_together = ('date', 'meal')


class Food(models.Model):
    attendee = models.OneToOneField(Attendee, related_name='food')

    meals = models.ManyToManyField(Meal)
    diet = models.CharField(max_length=16, blank=True)
    special_diet = models.TextField(blank=True)

    def __str__(self):
        return 'Attendee <{}>'.format(self.attendee.user.username)

    def invoice_data(self):
        """Generate one invoice line per meal type per consecutive stay"""

        for meal, meal_label in Meal.MEAL_CHOICES:
            dates = [entry.date for entry in self.meals.filter(meal=meal)]
            if not dates:
                continue

            unit_price = Meal.MEAL_PRICES[meal]

            ranges = get_ranges_for_dates(dates)
            for first, last in ranges:
                n_meals = (last - first).days + 1

                if first != last:
                    desc = "%s to %s" % (first, last)
                else:
                    desc = str(first)

                full_desc = 'DebConf 17 %s (%s)' % (meal_label, desc)
                yield {
                    'reference': 'DC17-%s' % meal.upper(),
                    'description': full_desc,
                    'unit_price': unit_price,
                    'quantity': n_meals,
                }
