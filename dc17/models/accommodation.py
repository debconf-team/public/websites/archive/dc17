import datetime

from django.db import models

from dc17.dates import get_ranges_for_dates
from dc17.models.attendee import Attendee


class AccommNight(models.Model):
    date = models.DateField(unique=True)

    @property
    def form_name(self):
        return 'night_{}'.format(self)

    def __str__(self):
        return self.date.isoformat()


class Accomm(models.Model):
    attendee = models.OneToOneField(Attendee, related_name='accomm')

    nights = models.ManyToManyField(AccommNight)
    requirements = models.TextField(blank=True)
    alt_accomm_choice = models.CharField(max_length=16, null=True, blank=True)
    childcare = models.BooleanField()
    childcare_needs = models.TextField(blank=True)
    childcare_details = models.TextField(blank=True)
    special_needs = models.TextField(blank=True)
    family_usernames = models.TextField(blank=True)
    onsite_room = models.CharField(max_length=128, blank=True, default='')
    rvc_room = models.CharField(max_length=128, blank=True, default='')

    def __str__(self):
        return 'Accomm <{}>'.format(self.attendee.user.username)

    def invoice_data(self):
        """Generate one invoice line per consecutive stay"""
        stays = get_ranges_for_dates(
            night.date for night in self.nights.all()
        )

        for first_night, last_night in stays:
            last_morning = last_night + datetime.timedelta(days=1)
            num_nights = (last_morning - first_night).days
            desc = "evening of %s to morning of %s" % (first_night,
                                                       last_morning)

            yield {
                'reference': 'DC17-ACCOMM',
                'description': 'DebConf 17 on-site accommodation (%s)' % desc,
                'unit_price': 30,
                'quantity': num_nights,
            }

    def save(self, *args, **kwargs):
        if self.alt_accomm_choice == '':
            self.alt_accomm_choice = None
        return super().save(*args, **kwargs)

    def get_checkin_checkouts(self):
        """Get the successive check-in and check-out dates for the attendee"""
        stays = get_ranges_for_dates(
            night.date for night in self.nights.all()
        )

        for first_night, last_night in stays:
            yield first_night
            yield last_night + datetime.timedelta(days=1)

    def get_stay_details(self):
        """Get the check-in, check-out and location for each stay"""
        alt_accomm = self.alt_accomm_choice
        ci_co = iter(self.get_checkin_checkouts())
        # pairwise
        for checkin, checkout in zip(ci_co, ci_co):
            if not alt_accomm:
                yield checkin, checkout, ''
                continue
            if checkin >= datetime.date(2017, 8, 4):
                yield checkin, checkout, alt_accomm
                continue
            else:
                cutoff = datetime.date(2017, 8, 5)
                yield checkin, min(cutoff, checkout), ''
                if cutoff < checkout:
                    yield cutoff, checkout, alt_accomm
