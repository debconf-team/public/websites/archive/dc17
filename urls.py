from django.conf.urls import include, url


urlpatterns = [
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^invoices/', include('invoices.urls')),
    url(r'^volunteers/', include('volunteers.urls')),
    url(r'^front_desk/', include('front_desk.urls')),
    url(r'', include('dc17.urls')),
    url(r'', include('wafer.urls')),
]
