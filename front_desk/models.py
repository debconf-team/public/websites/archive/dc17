from django.db import models

from dc17.models import Attendee


class CheckIn(models.Model):
    attendee = models.OneToOneField(Attendee, related_name='check_in')
    t_shirt_cut = models.CharField(max_length=1, blank=True)
    t_shirt_size = models.CharField(max_length=8, blank=True)
    swag = models.BooleanField()
    nametag = models.BooleanField()
    debcamp_meal_card = models.BooleanField()
    debconf_meal_card = models.BooleanField()
    stm_pass = models.BooleanField()
    room_key = models.BooleanField()
    notes = models.TextField(blank=True)
