# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dc17', '0013_room_assignations'),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckIn',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('t_shirt_cut', models.CharField(max_length=1, blank=True)),
                ('t_shirt_size', models.CharField(max_length=8, blank=True)),
                ('swag', models.BooleanField()),
                ('nametag', models.BooleanField()),
                ('meal_card', models.BooleanField()),
                ('notes', models.TextField(blank=True)),
                ('attendee', models.OneToOneField(to='dc17.Attendee')),
            ],
        ),
    ]
