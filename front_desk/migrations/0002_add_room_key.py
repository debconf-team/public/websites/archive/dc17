# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('front_desk', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkin',
            name='room_key',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='checkin',
            name='attendee',
            field=models.OneToOneField(to='dc17.Attendee', related_name='check_in'),
        ),
    ]
