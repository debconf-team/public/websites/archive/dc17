# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('front_desk', '0002_add_room_key'),
    ]

    operations = [
        migrations.RenameField(
            model_name='checkin',
            old_name='meal_card',
            new_name='debcamp_meal_card',
        ),
        migrations.AddField(
            model_name='checkin',
            name='debconf_meal_card',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='checkin',
            name='stm_pass',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
