import datetime

from django.contrib import admin
from django.contrib.admin.templatetags.admin_list import _boolean_icon
from django.db.models import Q

from django.utils.html import format_html, format_html_join
from django.utils.http import urlquote

from dc17.models import Attendee
from front_desk.models import CheckIn

class NotNullFilter(admin.SimpleListFilter):
    title = 'Filter title not set'

    parameter_name = 'parameter name not set'

    def lookups(self, request, model_admin):

        return (
            ('not_null', 'Yes'),
            ('null', 'No'),
        )

    def queryset(self, request, queryset):

        if self.value() == 'not_null':
            is_null_false = {
                self.parameter_name + "__isnull": False
            }
            exclude = {
                self.parameter_name: ""
            }
            return queryset.filter(**is_null_false).exclude(**exclude)

        if self.value() == 'null':
            is_null_true = {
                self.parameter_name + "__isnull": True
            }
            param_exact = {
                self.parameter_name + "__exact": ""
            }
            return queryset.filter(Q(**is_null_true) | Q(**param_exact))


# FRONT DESK

class TShirtNotNullFilter(NotNullFilter):
    title = "T-Shirt"
    parameter_name = "t_shirt_cut"


class CheckInAdmin(admin.ModelAdmin):
    list_display = (
        'attendee', 'full_name', 'email', 'swag', 'nametag',
        'debcamp_meal_card', 'debconf_meal_card', 'stm_pass', 'room_key',
    )
    list_filter = (
        TShirtNotNullFilter, 'swag', 'nametag', 'debcamp_meal_card',
        'debconf_meal_card', 'stm_pass', 'room_key',
    )
    search_fields = (
        'attendee__user__username', 'attendee__user__first_name',
        'attendee__user__last_name'
    )

    def full_name(self, instance):
        return instance.attendee.user.get_full_name()
    full_name.admin_order_field = 'attendee__user__last_name'

    def email(self, instance):
        return instance.attendee.user.email
    email.admin_order_field = 'attendee__user__email'


admin.site.register(CheckIn, CheckInAdmin)
