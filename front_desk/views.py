from collections import OrderedDict
from datetime import date, datetime
from itertools import repeat

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormView

from wafer.utils import LoginRequiredMixin

from dc17.models import Accomm, Attendee, Food, Meal
from dc17.forms import parse_date
from front_desk.forms import (
    CashInvoicePaymentForm, CheckInForm, FoodForm, RegisterOnSiteForm,
    SearchForm, TShirtForm)
from front_desk.models import CheckIn
from invoices.models import Invoice


class CheckInPermissionMixin(LoginRequiredMixin):
    @method_decorator(permission_required('front_desk.change_checkin'))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class CashBoxPermissionMixin(LoginRequiredMixin):
    @method_decorator(permission_required('invoices.change_invoice'))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class Dashboard(CheckInPermissionMixin, TemplateView):
    template_name = 'front_desk/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = SearchForm(data=self.request.GET)

        q = None
        if form.is_valid():
            q = form.cleaned_data['q']

        results = self.search_attendees(q)
        # Strip duplicates while maintaining order
        results = list(OrderedDict(zip(results, repeat(None))))

        context.update({
            'view': self,
            'form': form,
            'results': results[:20],
            'num_results': len(results),
        })
        return context

    def search_attendees(self, query):
        if not query:
            return

        yield from Attendee.objects.filter(user__username=query)

        if '@' in query:
            yield from Attendee.objects.filter(user__email=query)

        if ' ' in query:
            first, last = query.split(' ', 1)
            yield from Attendee.objects.filter(
                user__first_name__iexact=first,
                user__last_name__iexact=last,
            )
        yield from Attendee.objects.filter(user__username__icontains=query)
        yield from Attendee.objects.filter(user__email__icontains=query)
        yield from Attendee.objects.filter(user__first_name__icontains=query)
        yield from Attendee.objects.filter(user__last_name__icontains=query)


class CheckInView(CheckInPermissionMixin, SingleObjectMixin, FormView):
    template_name = 'front_desk/check_in.html'
    form_class = CheckInForm

    model = Attendee
    slug_field = 'user__username'
    slug_url_kwarg = 'username'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        room_key = False
        try:
            if self.object.accomm.onsite_room:
                room_key = True
        except Accomm.DoesNotExist:
            pass
        kwargs['room_key'] = room_key
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        attendee = self.object
        initial_tshirt = {
            'size': attendee.t_shirt_size,
            'cut': attendee.t_shirt_cut,
        }
        try:
            check_in = attendee.check_in
            if check_in.t_shirt_size and check_in.t_shirt_cut:
                initial_tshirt = {
                    'size': check_in.t_shirt_size,
                    'cut': check_in.t_shirt_cut,
                }
        except CheckIn.DoesNotExist:
            pass
        try:
            accomm = attendee.accomm
        except Accomm.DoesNotExist:
            accomm = None

        dates = [date(2017, 7, 31)] + [date(2017, 8, x) for x in range(1, 14)]
        meal_labels = ['Breakfast', 'Lunch', 'Dinner']

        try:
            food = attendee.food
        except Food.DoesNotExist:
            meals = None
        else:
            meals = []
            for meal, meal_label in Meal.MEAL_CHOICES:
                cur_meal = []
                meal_dates = [entry.date
                              for entry in food.meals.filter(meal=meal)]
                for d in dates:
                    cur_meal.append((d, d in meal_dates))
                meals.append((meal_label, cur_meal))

        context.update({
            'accomm': accomm,
            'invoices': Invoice.objects.filter(recipient=attendee.user),
            'dates': dates,
            'meal_labels': meal_labels,
            'meals': meals,
            't_shirt_form': TShirtForm(
                initial=initial_tshirt,
                username=attendee.user.username),
        })
        return context

    def get_initial(self):
        try:
            check_in = self.object.check_in
        except CheckIn.DoesNotExist:
            return {}
        return {
            't_shirt': check_in.t_shirt_size and check_in.t_shirt_cut,
            'nametag': check_in.nametag,
            'debcamp_meal_card': check_in.debcamp_meal_card,
            'debconf_meal_card': check_in.debconf_meal_card,
            'stm_pass': check_in.stm_pass,
            'notes': check_in.notes,
            'room_key': check_in.room_key,
            'swag': check_in.swag,
        }

    def form_valid(self, form):
        attendee = self.object
        try:
            check_in = attendee.check_in
        except CheckIn.DoesNotExist:
            check_in = CheckIn(attendee=attendee)

        if form.cleaned_data['t_shirt']:
            if not check_in.t_shirt_size:
                check_in.t_shirt_size = attendee.t_shirt_size
            if not check_in.t_shirt_cut:
                check_in.t_shirt_cut = attendee.t_shirt_cut
        else:
            check_in.t_shirt_size = ''
            check_in.t_shirt_cut = ''
        for field in ('nametag', 'debcamp_meal_card', 'debconf_meal_card',
                      'stm_pass', 'notes', 'room_key', 'swag'):
            setattr(check_in, field, form.cleaned_data[field])
        check_in.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('front_desk')


class ChangeFoodView(CashBoxPermissionMixin, SingleObjectMixin, FormView):
    form_class = FoodForm
    template_name = 'front_desk/change_food.html'

    model = Attendee
    slug_field = 'user__username'
    slug_url_kwarg = 'username'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.username = self.kwargs['username']
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.username = self.kwargs['username']
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.object
        try:
            food = self.object.food
        except Food.DoesNotExist:
            pass
        else:
            kwargs['initial'] = {
                'meals': [meal.form_name for meal in food.meals.all()],
            }
        return kwargs

    def get_success_url(self):
        return reverse('front_desk.check_in',
                       kwargs={'username': self.username})

    def form_valid(self, form):
        data = form.cleaned_data
        if data['meals']:
            food, created = Food.objects.get_or_create(
                attendee=self.object)
            stored_meals = set(food.meals.all())
            requested_meals = set()
            for meal in data['meals']:
                meal, date = meal.split('_')
                date = parse_date(date)
                requested_meals.add(Meal.objects.get(meal=meal, date=date))

            food.meals.remove(*(stored_meals - requested_meals))
            food.meals.add(*(requested_meals - stored_meals))
        else:
            Food.objects.filter(attendee=self.object).delete()

        return super().form_valid(form)


class ChangeShirtView(CheckInPermissionMixin, FormView):
    form_class = TShirtForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.username = self.kwargs['username']
        kwargs['username'] = self.username
        return kwargs

    def get_success_url(self):
        return reverse('front_desk.check_in',
                       kwargs={'username': self.username})

    def form_valid(self, form):
        attendee = Attendee.objects.get(user__username=self.username)
        try:
            check_in = attendee.check_in
        except CheckIn.DoesNotExist:
            check_in = CheckIn(attendee=attendee)
        check_in.t_shirt_size = form.cleaned_data['size']
        check_in.t_shirt_cut = form.cleaned_data['cut']
        check_in.save()

        return super().form_valid(form)


class RegisterOnSite(CheckInPermissionMixin, FormView):
    form_class = RegisterOnSiteForm
    template_name = 'front_desk/register.html'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        self.username = username
        email = form.cleaned_data['email']

        names = form.cleaned_data['name'].split(None, 1)
        if len(names) > 1:
            first_name, last_name = names
        else:
            first_name, last_name = names[0], ''

        user = get_user_model().objects.create_user(
            username=username, email=email, first_name=first_name,
            last_name=last_name)
        Attendee.objects.create(
            user=user, nametag_3=username, arrival=datetime.now(),
            departure=form.cleaned_data['departure'], announce_me=False,
            register_announce=False, register_discuss=False,
            debcamp=False, open_day=False, debconf=False, final_dates=True,
            reconfirm=True)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('front_desk.check_in',
                       kwargs={'username': self.username})


class CashInvoicePayment(CashBoxPermissionMixin, SingleObjectMixin, FormView):
    form_class = CashInvoicePaymentForm
    template_name = 'front_desk/cash_invoice_payment.html'

    model = Invoice
    slug_field = 'reference_number'
    slug_url_kwarg = 'ref'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.object
        return kwargs

    def form_valid(self, form):
        invoice = self.object
        invoice.transaction_id = 'Cash: {}'.format(
            form.cleaned_data['receipt_number'])
        invoice.status = 'paid'
        invoice.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('front_desk.check_in',
                       kwargs={'username': self.object.recipient.username})
