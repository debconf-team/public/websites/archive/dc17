---
title: DebConf17 closes in Montreal and DebConf18 dates announced
---

Today, Saturday 12 August 2017, the annual Debian Developers 
and Contributors Conference came to a close. 
With over 405 people attending from all over the world
(see the [DebConf17 group photo][]), 
and 169 events including 89 talks, 61 discussion sessions or BoFs,
6 workshops and 13 other activities, 
[DebConf17][] has been hailed as a success.

<a href="https://annex.debconf.org/debconf-share/debconf17/photos/aigarius/debcond17%20group%20photo.jpg"><img src="https://annex.debconf.org/debconf-share/debconf17/photos/aigarius/debcond17%20group%20photo%20mobile.jpg" alt="DebConf17 group photo. Click to enlarge" width="100%"/></a>

Highlights included DebCamp with 117 participants,
the [Open Day][], where events of interest to a broader audience were offered, 
talks from invited speakers
([Deb Nicholson][], [Matthew Garrett][] and [Katheryn Sutter][]),
the traditional Bits from the DPL, lightning talks and live demos 
and the announcement of next year's DebConf ([DebConf18][] in Hsinchu, Taiwan).


The [schedule](/schedule/)
has been updated every day, including 32 ad-hoc new activities, planned 
by attendees during the whole conference. 


For those not able to attend, talks and sessions were recorded and live streamed, 
and videos are being made available at the 
[Debian meetings archive website][].
Many sessions also facilitated remote participation via IRC or a collaborative pad.


The [DebConf17][] website 
will remain active for archive purposes, and will continue to offer
 links to the presentations and videos of talks and events.


Next year, [DebConf18][] will be held in Hsinchu, Taiwan, from 29 July 2018
until 5 August 2018. It will be the first DebConf held in Asia.
For the days before DebConf the local organisers will again set up DebCamp
(21 July - 27 July),
a session for some intense work on improving the distribution, 
and the Open Day on 28 July 2018, aimed at the general public.



DebConf is committed to a safe and welcome environment for all participants. 
See the [DebConf Code of Conduct][] 
and the [Debian Code of Conduct][] for more details on this.

Debian thanks the commitment of numerous [sponsors](/sponsors/) 
to support DebConf17, particularly our Platinum Sponsors 
[**Savoir-Faire Linux**][],
[**Hewlett Packard Enterprise (HPE)**][],
and [**Google**][].



## About Savoir-faire Linux

[**Savoir-Faire Linux**][]
is a Montreal-based Free/Open-Source Software company
with offices in Quebec City, Toronto, Paris and Lyon. It offers Linux and
Free Software integration solutions in order to provide performance,
flexibility and independence for its clients. The company actively contributes
to many free software projects, and provides mirrors of Debian, Ubuntu, Linux
and others. 

## About Hewlett Packard Enterprise

[**Hewlett Packard Enterprise (HPE)**][]
is one of the largest computer companies in the
world, providing a wide range of products and services, such as servers, storage, 
networking, consulting and support, software, and financial services.

HPE is also a development partner of Debian, 
and provides hardware for port development, Debian mirrors, and other Debian services.


## About Google

[**Google**][]
is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products
as online advertising technologies, search, cloud computing, software, and hardware. 

Google has been supporting Debian by sponsoring DebConf since more than
ten years, at gold level since DebConf12, and at platinum level for this DebConf17.


This news item was originally posted in the [Debian blog][].

[DebConf17 group photo]: https://annex.debconf.org/debconf-share/debconf17/photos/aigarius/debcond17%20group%20photo.jpg
[**Savoir-Faire Linux**]: https://www.savoirfairelinux.com/
[**Hewlett Packard Enterprise (HPE)**]: http://www.hpe.com/engage/opensource
[**Google**]: https://google.com/
[DebConf17]: https://debconf17.debconf.org/
[Open Day]: https://debconf17.debconf.org/news/2017-08-05-open-day/
[Deb Nicholson]: https://debconf17.debconf.org/talks/183/ 
[Matthew Garrett]: https://debconf17.debconf.org/talks/177/
[Katheryn Sutter]: https://debconf17.debconf.org/talks/134/
[Debian meetings archive website]: http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17
[Debian blog]: https://bits.debian.org/2017/08/debconf17-closes.html
[DebConf Code of Conduct]: http://debconf.org/codeofconduct.shtml
[Debian Code of Conduct]: https://www.debian.org/code_of_conduct
[**#debconf17-rex**]: https://webchat.oftc.net/?channels=#debconf17-rex
[**#debconf17-potato**]: https://webchat.oftc.net/?channels=#debconf17-potato
[**#debconf17-woody**]: https://webchat.oftc.net/?channels=#debconf17-woody
[**#debconf17-buzz**]: https://webchat.oftc.net/?channels=#debconf17-buzz
[**#debconf17-bo**]: https://webchat.oftc.net/?channels=#debconf17-bo
