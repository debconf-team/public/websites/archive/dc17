---
title: Call for Proposals for DebConf17 Open Day
---

The Call for Proposals for DebConf17 Open Day has been [published][].

[published]: /schedule/open-day/

You can now get sign in in this website and submit an event. The schedule
 will be published in June.

We hope to see you in Montreal!
