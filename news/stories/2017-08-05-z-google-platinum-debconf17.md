---
title: Google Platinum Sponsor of DebConf17
---

We are very pleased to announce that [**Google**][] 
has committed support to [DebConf17][] as a **Platinum sponsor**.

Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products
as online advertising technologies, search, cloud computing, software, and hardware. 

Google has been supporting Debian by sponsoring DebConf since more than
ten years, and at gold level since DebConf12.

With this additional commitment as Platinum Sponsor for DebConf17, 
Google contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software 
helping to strengthen the community that continues to collaborate on 
Debian projects throughout the rest of the year.

Thank you very much Google, for your support of DebConf17!

## DebConf17 is starting!

Many Debian contributors are already taking advantage of DebCamp
and the Open Day 
to work individually or in groups developing and improving Debian.
DebConf17 will officially start on August 6, 2017.
Visit the DebConf17 website 
to know the schedule, live streaming and other details.


This news item was originally posted in the [Debian blog][].

[**Google**]: https://google.com/
[DebConf17]: https://debconf17.debconf.org/
[Debian blog]: https://bits.debian.org/2017/08/google-platinum-debconf17.html
