---
title: DebConf17 Schedule Published!
---

The DebConf17 orga team is proud to announce that over 120 activities
have been scheduled so far, including 45- and 20-minute talks, team
meetings, and workshops, as well as a variety of other events.

Most of the talks and BoFs will be streamed and recorded, thanks to our
amazing video team!

We'd like to remind you that Saturday August 5th is also our much
anticipated Open Day! This means a program for a wider audience,
including special activities for newcomers, such as an AMA session about
Debian, a beginners workshop on packaging, a thoughtful talk about
freedom with regard to today's popular gadgets and more.

In addition to the published schedule, we'll provide rooms for ad-hoc
sessions where attendees will be able to schedule activities at any time
during the whole conference.

The current schedule is available [here](/schedule/).

This is also available through an [XML feed](/schedule/mobile/). You can
use ConfClerk in Debian to consume this, or Giggity on Android devices.

We look forward to seeing you in Montreal!
