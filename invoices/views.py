import itertools
import re

from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse
from django.views.generic import DetailView
from django.views.generic.base import View

from paypal.standard.forms import PayPalPaymentsForm
from wafer.utils import LoginRequiredMixin

from invoices.models import Invoice


class InvoiceDisplayMixin(LoginRequiredMixin, DetailView):
    model = Invoice
    slug_field = 'reference_number'
    slug_url_kwarg = 'reference_number'

    def get_object(self):
        ret = super().get_object()

        user = self.request.user
        if (user.has_perm('front_desk.change_checkin')
                or ret.recipient == user):
            return ret
        else:
            raise Http404()


class InvoiceDisplay(InvoiceDisplayMixin):
    template_name = 'invoices/invoice_detail.html'

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(*args, **kwargs)

        paypal_data = self.object.paypal_data()
        paypal_data['notify_url'] = self.request.build_absolute_uri(
            reverse('paypal-ipn')
        )
        paypal_data['return_url'] = self.request.build_absolute_uri(
            reverse(
                'invoice_display',
                kwargs={
                    'reference_number': self.kwargs['reference_number']
                },
            )
        )

        context_data['form'] = PayPalPaymentsForm(initial=paypal_data)

        return context_data


class InvoicePrint(InvoiceDisplayMixin):
    template_name = 'invoices/print.html'


class InvoiceLedger(View):
    def ledger_lines(self, invoice):
        mealRE = re.compile(r'(?i)(Breakfast|Lunch|Dinner|Supper)')
        yield '{:%Y-%m-%d} * Attendee invoice #{}'.format(
            invoice.last_update,
            invoice.reference_number)
        yield '  ; private-invoices/{:%Y%m%d}_invoice_{}.pdf'.format(
            invoice.date,
            invoice.reference_number)
        for line in invoice.lines.all():
            account = None
            if 'registration' in line.description:
                account = 'income:attendee:registration'
            elif 'on-site accommodation' in line.description:
                account = 'income:roomboard:debconf:classrooms'
            elif mealRE.search(line.description):
                account = 'income:roomboard:debconf:food'
            if not account:
                raise Exception('Unhandled line item: {}'.format(
                    line.description))
            yield '  {:37} {: 8.2f} CAD'.format(
                account,
                -line.total)
        yield '  assets:receivable'
        yield ''

    def get(self, request, *args, **kwargs):
        lines = itertools.chain.from_iterable(
            self.ledger_lines(invoice)
            for invoice in
            Invoice.objects.filter(status='paid').order_by('last_update'))
        return HttpResponse('\n'.join(lines), content_type='text/plain')
