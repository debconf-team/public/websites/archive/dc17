# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from invoices.models import Invoice

SUBJECT = ('Your DebConf17 attendance invoice is outstanding!')

TXT = """\
Dear %(name)s

Our records indicate that you haven't settled your DebConf17 invoice, for a
total of %(total)s CAD.

You can settle the invoice directly online, by going to your profile on the
DebConf website[1].

[1] https://debconf17.debconf.org/accounts/profile/

The invoice details are as follows:

Invoice number #%(invoice_number)s

%(invoice_details)s

Thanks for your support,
The DebConf17 Registration Team
"""


class Command(BaseCommand):
    help = 'Badger outstanding invoices'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, invoice, dry_run):
        to = invoice.recipient.email
        name = invoice.recipient.get_full_name()
        invoice_number = invoice.reference_number
        total = invoice.total

        if not total:
            return

        if dry_run:
            print('I would badger %s <%s> (%s: %s CAD)' %
                  (name, to, invoice_number, total))
            return

        body = TXT % {
            'name': name,
            'total': total,
            'invoice_number': invoice_number,
            'invoice_details': invoice.text_details(),
        }

        email_message = EmailMultiAlternatives(SUBJECT, body,
                                               to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        for invoice in Invoice.objects.filter(status='draft'):
            self.badger(invoice, dry_run)
