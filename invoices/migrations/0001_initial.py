# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('reference_number', models.CharField(max_length=128, unique=True)),
                ('status', models.CharField(default='draft', max_length=128, choices=[('draft', 'Draft'), ('pending', 'Payment pending'), ('paid', 'Payment received'), ('canceled', 'Invoice canceled')])),
                ('date', models.DateField()),
                ('billing_address', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_update', models.DateTimeField(auto_now=True)),
                ('recipient', models.ForeignKey(related_name='invoices', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='InvoiceLine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('line_order', models.IntegerField()),
                ('reference', models.CharField(max_length=32)),
                ('description', models.CharField(max_length=1024)),
                ('unit_price', models.DecimalField(validators=[django.core.validators.MinValueValidator(0)], max_digits=8, decimal_places=2)),
                ('quantity', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('invoice', models.ForeignKey(related_name='lines', to='invoices.Invoice')),
            ],
            options={
                'ordering': ('invoice', 'line_order'),
            },
        ),
        migrations.AlterUniqueTogether(
            name='invoiceline',
            unique_together=set([('invoice', 'line_order')]),
        ),
    ]
