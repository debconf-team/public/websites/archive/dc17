from django.conf.urls import url

from invoices.views import InvoiceDisplay, InvoicePrint, InvoiceLedger

urlpatterns = [
    url(r'^(?P<reference_number>[^/]+)/$', InvoiceDisplay.as_view(),
        name='invoice_display'),
    url(r'^(?P<reference_number>[^/]+)/print/$', InvoicePrint.as_view(),
        name='invoice_print'),
    url(r'^paid.ledger$', InvoiceLedger.as_view())
]
