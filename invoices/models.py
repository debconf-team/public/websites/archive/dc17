from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.validators import MinValueValidator
from django.db import models
from django.dispatch import receiver

from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received


class Invoice(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('pending', 'Payment pending'),
        ('paid', 'Payment received'),
        ('canceled', 'Invoice canceled'),
    )

    reference_number = models.CharField(max_length=128, unique=True,
                                        null=False, blank=False)
    status = models.CharField(max_length=128, choices=STATUS_CHOICES,
                              default='draft')
    date = models.DateField()

    recipient = models.ForeignKey(settings.AUTH_USER_MODEL,
                                  related_name='invoices')
    billing_address = models.TextField()

    transaction_id = models.CharField(max_length=128, null=False, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    @property
    def total(self):
        return sum(line.total for line in self.lines.all())

    def paypal_data(self):
        data = settings.PAYPAL_DATA.copy()

        data['cmd'] = '_cart'
        data['invoice'] = settings.INVOICE_PREFIX + self.reference_number
        data['upload'] = '1'

        for i, line in enumerate(self.lines.all()):
            i = i + 1
            data['item_name_%d' % i] = line.description
            data['quantity_%d' % i] = line.quantity
            data['amount_%d' % i] = line.unit_price

        return data

    def text_details(self):
        header = ('Reference', 'Description', 'Qty', 'Unit', 'Total')
        footer = ('', '', '', 'Total', str(self.total))
        lines = [
            (line.reference, line.description, line.quantity, line.unit_price,
             line.total)
            for line in self.lines.all()
        ]
        all_lines = (
            [
                header,
                ('', '', '', '', ''),
            ]
            + lines
            + [
                ('', '', '', '-------', '-------'),
                footer,
            ]
        )

        col_width = [max(len(str(x)) for x in col) for col in zip(*all_lines)]
        formats = ['{:{}}'] * 3 + ['{:>{}}'] * 2
        return "\n".join(
            ("| " + " | ".join(formats[i].format(x, col_width[i])
                               for i, x in enumerate(line)) + " |")
            for line in all_lines
        )

    def save(self, *args, **kwargs):
        # generate reference number on save
        if not self.reference_number:
            year = str(self.date.year)
            last_invoice = Invoice.objects.filter(
                reference_number__startswith=year
            ).order_by('-reference_number').first()

            if last_invoice:
                year, seqnum = last_invoice.reference_number.split('-')
                seqnum = int(seqnum, 10) + 1
            else:
                seqnum = 1

            self.reference_number = '%s-%05d' % (year, seqnum)

        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse(
            'invoice_display',
            kwargs={'reference_number': self.reference_number})


@receiver(valid_ipn_received)
def update_invoice_on_ipn_received(sender, **kwargs):
    ipn = sender
    paypal_data = settings.PAYPAL_DATA
    if ipn.payment_status == ST_PP_COMPLETED:
        if ipn.receiver_email != paypal_data['business']:
            return
        if not ipn.invoice.startswith(settings.INVOICE_PREFIX):
            return

        invoice_number = ipn.invoice[len(settings.INVOICE_PREFIX):]
        invoice = Invoice.objects.get(reference_number=invoice_number)
        invoice.status = 'paid'
        invoice.transaction_id = ipn.txn_id
        invoice.save()


class InvoiceLine(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='lines')
    line_order = models.IntegerField()
    reference = models.CharField(max_length=32)
    description = models.CharField(max_length=1024)
    unit_price = models.DecimalField(max_digits=8, decimal_places=2,
                                     validators=[MinValueValidator(0)])
    quantity = models.IntegerField(validators=[MinValueValidator(1)])

    @property
    def total(self):
        return self.unit_price * self.quantity

    class Meta:
        unique_together = ('invoice', 'line_order')
        ordering = ('invoice', 'line_order')

    def __str__(self):
        return 'InvoiceLine(%s [%s] %d @ %.02f = %.02f)' % (
            self.reference,
            self.description,
            self.quantity,
            self.unit_price,
            self.total,
        )
